#!/bin/sh

echo "Validating rowwise against restricted Contiot schema..."
xmllint --schema ./xml/contiot.xsd ./xml/minimal_example.xml --noout
if [ $? -ne 0 ]; then
  exit 1
fi

echo ""
echo "Validating rowwise against original PTB schema..."
xmllint --schema ./xml/dcc.xsd ./xml/minimal_example.xml --noout
if [ $? -ne 0 ]; then
  exit 1
fi

echo "Done!"
