# Running tests

You can either run ```sh test.sh``` 

or build the alpine Docker image with ```docker build -t "test" && docker run test```

# Changes to the XSD

### dcc:restrictedDigitalCalibrationCertificateType
- replaces digitalCalibrationCertificateType
- changed /administrativeData type from administrativeDataType to restricedAdministrativeDataType
- changed /measurementResults type from mreasurementResultListType to restrictedMeasurementResultListType
- removed /document

## Changes to the measurement data

### dcc:restrictedMeasurementResultListType
- replaces measurementResultListType
- changed /measurementResult type from measurementResultType to restrictedMeasurementResultType
- changed /result occurrences from [1, inf) to [1, 1]

### dcc:restrictedUsedMethodType
- replaces usedMethodType
- changed /name to from textype to restrictedNameType
- changed /description to dcc:restrictedTextType
- changed /norm occurences to [0, 1]

### dcc:restrictedMeasurementResultType
- replaces measurementResultType
- removed /usedSoftware
- removed /usedMethods
- removed /influenceConditions

### dcc:restrictedMeasuringEquipmentType
- replaces measuringEquipmentType
- /descriptionData deleted
- /description occurence to [1, 1]
- /name type changed to restrictedTextType
- /identifications type changed from identificationListType to restrictedIdentitificationListType
- /equipmentClass deleted
- changed /manufacturer type from contactNotStrictType to restrictedContactNotStrictType
- changed /certificate type from hashType to restrictedCertificateType

### dcc:restrictedCertificateType
- replaces dcc:hashType
- /reference type changed to restrictedTextType
- removed /linkedReport

### dcc:restrictedInfluenceConditionListType
- replaces influenceConditionType
- changed influenceCondition type from conditionType to restrictedConditionType
- changed influenceCondition occurrences from [1, inf) to [1, 1]

### dcc:restrictedConditionType
- replaces conditionType
- changed /name type from textType to restrictedTextType
- changed /description type from textType to restrictedTextType
- changed /data type from dataType to restrictedConditionDataType

### dcc:restrictedConditionDataType
- cloned from dataType
- removed /byteData
- removed /xml
- removed /list
- removed /formula
- removed /text
- changed xs:choice occurrence from [1, inf) to [1, 1]
- changed /quantity type from [1, 1] to [1, inf)

### dcc:restrictedDataType
- cloned from dataType
- removed /byteData
- removed /xml
- changed xs:choice to xs:sequence
- changed xs:sequence occurrences from [1, inf) to [1, 1]
- changed /list type from listType to restrictedListType
- changed /list occurrences from [1, 1] to [1, inf)
- removed /quantity
- removed /formula
- removed /text

### dcc:restrictedListType
- replaces listType
- removed /usedSoftware
- removed /measuringEquipments
- removed /xs:choice/* (list and quantity)
- added /quantity with type restrictedQuantityType and occurrences [1, inf)
- changed /dateTime occurrences from [1, 1] to [0, 1]
- changed /influenceConditions type from influenceConditionListType to restrictedInfluenceConditionListType
- removed /name

### dcc:restrictedInfluenceQuantityType
- replaces quantityType
- removed /influenceConditions
- removed /noQuantity
- changed /name type from textType to restrictedTextType
- removed /usedMethods
- removed /usedSoftware
- removed /measurementMetaData
- removed /si:list
- removed /si:hybrid
- removed /xs:sequence/xs:choice

### dcc:restrictedQuantityType
- cloned from quantityType
- removed everything
- added /si:real with occurrences [1, 1]
- added /name with occurrences [1, 1]

### dcc:restrictedResultType
- replaces resultType
- changed /name type from textType to restrictedTextType
- changed /description type from textType to restrictedTextType
- changed /data type from dataType to restrictedDataType



## Changes to the Administrative data

### dcc:restrictedAdministrativeDataType
- replaces administrativeDataType
- changed all textTypes to restrictedTextTypes
- changed /dccSoftware type from softwareListType to restrictedSoftwareListType
- changed /coreData type from coreDataType to restrictedCoreDataType
- changed /items type from itemListType to restrictedItemListType
- changed /customer type from contactType to restrictedContactType
- changed /statements type from statementListType to restrictedStatementListType

### dcc:restrictedTextType
- cloned from textType
- changed /content type from stringWithLangType to xs:string
- changed /content occurrences to [1, 1]

### dcc:restrictedCoreDataType
- replaces coreDataType
- changed /usedLangCodeISO639_1 occurrences from [1, inf) to [1, 1]
- changed /mandatoryLangCodeISO639_1 occurrences from [1, inf) to [1, 1]
- changed /identifications type from identificationListType to restrictedIdentificationListType
- removed /previousReport
- removed /receiptDate

### dcc:restrictedStatementListType
- replaces statementListType
- changed /statement type from statementMetaDataType to restrictedStatementMetaDataType

### dcc:restrictedStatementMetaDataType
- cloned from statementMetaDataType
- changed /declaration occurrences from [0, 1] to [1, 1]
- changed /declaration type from textType to restrictedTextType
- removed /valid
- removed /period
- removed /date

### dcc:restrictedStateType
- new type for state in influence conditions
- added /state with restriction of xs:string (enum) with occurrences [1, 1]


### dcc:restrictedMeasurementMetaDataListType
- changed /metaData type from statementMetaDataType to restrictedMeasurementMetaDataType
- changed /metaData occurrences from [1, inf) to [1, 1]

### dcc:restrictedMeasurementMetaDataType
- cloned from statementMetaDataType
- removed everything
- added /declaration with type restrictedTextType and occurrences [1, 1]

### dcc:restrictedSoftwareListType
- cloned from softwareListType
- changed /software type from softwareType to restrictedSoftwareType
- changed /software occurrences from [1, inf) to [1, 1]

### dcc:restrictedSoftwareType
- cloned from softwareType
- changed /name type to restrictedTextType
- removed /description

### dcc:restrictedItemListType
- replaces itemListType
- changed /item type from itemType to restrictedItemType
- removed /name
- removed /equipmentClass
- removed /description
- removed /owner
- removed /identifications

### dcc:restrictedItemType
- replaces itemType
- changed /name type from textType to restrictedTextType
- changed /description occurrences from [1, inf) to [1, 1]
- changed /description type from textType to restrictedTextType
- changed /manufacturer type from contactNotStrictType to restrictedContactNotStrictType
- removed /descriptionData
- removed /equipmentClass

### dcc:restrictedIdentificationListType
- cloned from identificationListType
- changed /identification type from identificationType to restrictedIdentificationType

### dcc:restrictedIdentificationType
- cloned from identificationType
- removed /description

### dcc:restrictedContactNotStrictType
- cloned from contactNotStrictType
- removed /descriptionData
- changed /name type from textType to restrictedTextType
- changed /location type from locationType to restrictedLocationType

### dcc:restrictedContactType
- replaces contactType
- removed /descriptionData
- changed /name type from textType to restrictedTextType
- changed /location type from locationType to restrictedLocationType

### dcc:restrictedLocationType
- cloned from locationType
- changed choice to sequence with [1, 1] occurrences
- removed /further (valid?)
- changed /postOfficeBox occurrences from [1, 1] to [0, 1]
- changed /state occurrences from [1, 1] to [0, 1]
- changed /streetNo occurrences from [1, 1] to [0, 1]
- changed /city occurrences from [1, 1] to [0, 1]
- changed /postCode occurrences from [1, 1] to [0, 1]
- changed /street occurrences from [1, 1] to [0, 1]

### dcc:restrictedCalibrationLaboratoryType
- replaces calibrationLaboratoryType
- removed /calibrationLaboratoryCode
- changed /contact type from contactType to restrictedContactType
- changed /contact occurrences from [1, inf) to [1, 1]

### dcc:restrictedRespPersonListType
- replaces respPersonListType
- changed /respPerson type from respPersonType to restrictedRespPersonType

### dcc:restrictedRespPersonType
- replaces respPersonType
- changed /person type from contactNotStrictType to restrictedContactNotStrictType
- changed /description type from textType to restrictedTextType
- removed /mainSigner
- removed /cryptElectronicSignature
- removed /cryptElectronicSeal
- removed /cryptElectronicTimeStamp

# Notes:
- coreDataType has uniqueIdentifier which could be used for DCC ID
- locationType has mandatory /further ????
- restrictedIdentificationListType occurs was [1, 1] but was changed back to [1, inf) due to coreData identifications