## Explanations

### contiot.xsd
Subschema of PTB DCC schema, where measurements are given in simplified "measurmentpoint" format.

### contiot.json
JSON Schema generated from XSD

### minimal_example.xml
Minimal XML generated from contiot.xsd

### schema.json (DEPRECATED)
Old JSON Schema

<br>

### SI_Format.xsd
Used for local xmllint validation.
