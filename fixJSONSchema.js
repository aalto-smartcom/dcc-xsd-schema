const fs = require('fs')
const $RefParser = require("@apidevtools/json-schema-ref-parser");
const _ = require("lodash")
const { JSONPath } = require("jsonpath-plus");
const { exit } = require('process');

/** Flattens out allOfs from the schema so that the schema
 * won't break the form editor (a quirk of Oxygen XML editor)
 */
const removeAllOfs = (json) => {
  let copy = json
  const allOfPaths = 
    JSONPath({path: '$..allOf^', json, resultType: 'path'})
      .map(path => JSONPath.toPathArray(path).filter(str => str != '$'))

  allOfPaths.forEach(path => {
    _.update(copy, path, allOf => (
        {
          type: JSONPath({path: '$..type', json: allOf})[0],
          enum: JSONPath({path: '$..enum', json: allOf})[0]
        }
      )
    )
  })
  return copy
}

/** Transforms the si:real schemas so that only one of
 * expandedUnc and coverageInterval can be chosen, not both
*/
const fixSiReals = (json) => {
  let copy = json

  const siRealPaths = 
    JSONPath({path: '$..real', json, resultType: 'path'})
      .map(path => JSONPath.toPathArray(path).filter(str => str != '$'))

  if (siRealPaths.length > 0) {
    json["definitions"] = {
      expandedUnc: {
        type: "object",
        properties: {
          expandedUnc: JSONPath({path: '$..real', json})[0]["properties"]["expandedUnc"]
        }
      },
      coverageInterval: {
        type: "object",
        properties: {
          coverageInterval: JSONPath({path: '$..real', json})[0]["properties"]["coverageInterval"]
        }
      }
    }
  }

  siRealPaths.forEach(path => {
    _.update(copy, path, real => {
      const properties = real["properties"]
      delete properties["expandedUnc"]
      delete properties["coverageInterval"]

      return {
        type: "object",
        required: ["value", "unit"],
        oneOf: [
          { $ref: "#/definitions/expandedUnc" },
          { $ref: "#/definitions/coverageInterval" }
        ],
        properties
      }
    })
  })

  return copy
}


if (!process.argv[2]) {
  console.error("No file specified!")
  exit(1)
}
const filename = process.argv[2]
let json_restricted
try {
  json_restricted = fs.readFileSync(filename, { encoding: 'utf8', flag: 'r' })
} catch (error) {
  console.error("Error reading file", error)
  exit(1)
}


const jsonSchema = JSON.parse(json_restricted)

/**
 * Dereference the JSON schema
 */
$RefParser.dereference(jsonSchema, (err, schema) => {
  if (err) {
    console.error(err);
  }
  else {
    let json = schema.anyOf.find(x => x.properties.digitalCalibrationCertificate)
    json = 
//      fixSiReals(
        removeAllOfs(
          json
        )
//      )

    console.log(
      JSON.stringify(json, null, 2)
    )
  }
})
