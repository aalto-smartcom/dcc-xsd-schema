FROM alpine:3.13.0
RUN apk update && apk add libxml2 libxml2-utils
WORKDIR /usr/
COPY ./xml/SI_Format.xsd ./xml/
COPY ./xml/contiot.xsd ./xml/
COPY ./xml/dcc.xsd ./xml/
COPY ./xml/minimal_example.xml ./xml/
COPY test.sh .
CMD ["sh", "test.sh"]
